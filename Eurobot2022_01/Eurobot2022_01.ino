#include <TimerOne.h>
#include <math.h>

#define enableL 8
#define enableR 8

#define stepL 33
#define stepR 34

#define dirL 10
#define dirR 9

#define interruptPin 0







int t = 20; //ms Pause
int accelerationPhase = 100;          // die 100 ersten Steps
int accelerationNummer = 0.0025;      //multiplikator
int stepsDone = 0;
int i = 0;
int c = 0;    //test

volatile int time1 = 0;
volatile int time2 = 0;
volatile int time3 = 0;

volatile int dist1 = 0;
volatile int dist2 = 0;
volatile int dist3 = 0;

volatile int oldMillis = 0;



int triggerFront = 4; 
int echoFront = 5;
long dauerFront = 6; 
long entfernungFront=7; 

volatile int usTrigger = 0;
 

void setup() //Hier beginnt das Setup.
{
  Serial.begin(115200);
  pinMode(8, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(33, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(2, INPUT);

  pinMode(triggerFront, OUTPUT);
  pinMode(echoFront, INPUT);

  digitalWrite(enableR,HIGH); 
  digitalWrite(enableL,HIGH); 


  attachInterrupt(0,ultraschallTrigger,HIGH);

  Timer1.initialize(1*1000000);
  Timer1.attachInterrupt(frequenze);
  
}

void loop() 
{
  delay(50);
  Serial.println("Fahren");
  
}


   ////////////////////////////////////////////////////////////////////
  //                           Funktionen                           //
 ///////////////////////////////////////////////////////////////////




void frequenze()
{
   Serial.println("Auslesen");
   oldMillis = millis();
   
   
}



 void driveForward(int steps)
 {
    int a = 0; 
    
    digitalWrite(enableR,LOW); //oder low, habs vergessen
    digitalWrite(enableL,LOW); //oder low, habs vergessen

    digitalWrite(dirL,LOW);  //oder andersrum
    digitalWrite(dirR,LOW);  //oder andersrum


    for( stepsDone = 0; stepsDone < steps ; stepsDone++ )
    {    
        if(stepsDone < accelerationPhase && stepsDone <= steps/2) // Acceleration
        {
          a = 100 * 0.0025 *(100 - stepsDone);
          //Serial.println("+");
          //Serial.println(stepsDone);
          //Serial.println(a);
          
        }

        if (steps - stepsDone < accelerationPhase && stepsDone > steps / 2) // Decceleration
        {
          a = 100 * 0.0025 * (100 - (steps - stepsDone));
          //Serial.println("-");
          //Serial.println(stepsDone);
          //Serial.println(a);
        }

        digitalWrite(stepL,HIGH);
        digitalWrite(stepR,HIGH);
        delayMicroseconds(t + a);
        digitalWrite(stepL,LOW);
        digitalWrite(stepR,LOW);
        delayMicroseconds(t + a);
        Serial.println(t+a);
        
        
    }

    digitalWrite(enableR,HIGH); //oder low, habs vergessen
    digitalWrite(enableL,HIGH); //oder low, habs vergessen
 }

boolean uSonicfront()
{
  
  digitalWrite(triggerFront, LOW); 
  delay(5); 
  digitalWrite(triggerFront, HIGH); 
  delay(10);
  digitalWrite(triggerFront, LOW); dauerFront = pulseIn(echoFront, HIGH);
  entfernungFront = (dauerFront/2) * 0.03432; 
    if ( entfernungFront <= 0) 
    {
      Serial.println("Kein Messwert"); 
      return LOW;
    }
    else 
    {
      Serial.print(entfernungFront); 
      Serial.println(" cm");
      return HIGH; 
    }
 delay(1000); 
  
}


void brake()
{
  for(int i = 21; i >1; i--)
  {

        digitalWrite(stepL,HIGH);
        digitalWrite(stepR,HIGH);
        delayMicroseconds(i);
        digitalWrite(stepL,LOW);
        digitalWrite(stepR,LOW);
        delayMicroseconds(i);
        //Serial.println(i);
    
  }

}

void ultraschallTrigger()
{
  

  if (c == 0)
  {     
     time1 = millis() - oldMillis;
     dist1 = (time1/2) * 0.03432; 
     c++;
  }
  else if (c == 1)
  {
    time2 = millis() - oldMillis;
    dist2 = (time2/2) * 0.03432;
    c++;
  }

  else if (c == 1)
  {
    time3 = millis() - oldMillis;
    dist3 = (time3/2) * 0.03432;
    c = 0;
  }

  if(dist1 < 5 or dist2 < 5 or dist3 < 5 )
  {
      brake();
  }
    
}


void sonicTriggered()
{
  brake();
  Serial.println("Interrupt");
} 
